//
//  ViewController.swift
//  MinhaMusicas
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit

struct Musica{
    let nomeMusica : String
    let nomeAlbum : String
    let nomeCantor : String
    let nomeImagemGrande : String
    let nomeImagemPequena : String
}




class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var listaDeMusica:[Musica] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaDeMusica.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let musica = self.listaDeMusica[indexPath.row]
        
        cell.NomeDaMusica.text = musica.nomeMusica
        cell.NomeDoAlbum.text = musica.nomeAlbum
        cell.NomeDoCantor.text = musica.nomeCantor
        cell.Imagem.image = UIImage(named: musica.nomeImagemPequena)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "abrirDetalhe", sender: indexPath.row)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detalhesViewController = segue.destination as! DetalheMusicaViewController
        let indice = sender as! Int
        let musica = self.listaDeMusica[indice]
        
        detalhesViewController.nomeImagem = musica.nomeImagemGrande
        detalhesViewController.nomeMusica = musica.nomeMusica
        detalhesViewController.nomeAlbum = musica.nomeAlbum
        detalhesViewController.nomeCantor = musica.nomeCantor
    }
    

    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        self.listaDeMusica.append(Musica(nomeMusica: "Pontos Cardeias", nomeAlbum: "Álbum Vivo!", nomeCantor: "Alcaceu Valença", nomeImagemGrande: "capa_alceu_grande", nomeImagemPequena: "capa_alceu_pequeno"))
        self.listaDeMusica.append(Musica(nomeMusica: "Menor Abandonado", nomeAlbum: "Álbum Patota de Cosme", nomeCantor: "Zeca Pagodinho", nomeImagemGrande: "capa_zeca_grande", nomeImagemPequena: "capa_zeca_pequeno"))
        self.listaDeMusica.append(Musica(nomeMusica: "Tiro ao Álvaro", nomeAlbum: "Álbum Adoniran Barbosa e Convidados", nomeCantor: "Adoniran Barbosa", nomeImagemGrande: "capa_adhoniran_grande", nomeImagemPequena: "capa_adoniran_pequeno"))
        
        
        
    }


}

